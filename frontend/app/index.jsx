import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'

import App from './components/App'
import Home from './components/Home'
import Login from './components/Login'

const isBrowser = typeof window !== 'undefined'

export function createRouter(history) {
  return (
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="login" component={Login} />
      </Route>
    </Router>
  )
}

if (isBrowser) {
  document.addEventListener('DOMContentLoaded', () => {
    render(createRouter(browserHistory), document.getElementById('app'))
  })
}
