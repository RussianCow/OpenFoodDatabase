require('babel-polyfill')
require('babel-core/register')({
  presets: ['es2015', 'react'],
  plugins: [
    ['transform-regenerator', {
      generators: true
    }]
  ]
})

const server = require('./server')

if (!module.parent) {
  server.listen(8000, function() {
    console.log('Listening on port 8000...')
  })
}
