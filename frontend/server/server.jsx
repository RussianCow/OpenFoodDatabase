import fs from 'fs'
import http from 'http'

import cheerio from 'cheerio'
import React from 'react'
import { renderToStaticMarkup } from 'react-dom/server'
import { createMemoryHistory } from 'react-router'

import { createRouter } from '../app'


const html = fs.readFileSync('app/index.html', 'utf8')
const $ = cheerio.load(html)

const server = http.createServer((req, res) => {
  const history = createMemoryHistory()
  const location = history.createLocation(req.url)
  history.push(location)

  const router = createRouter(history)
  const appHtml = renderToStaticMarkup(router)
  $('#app').html(appHtml)
  const body = $.html()

  res.writeHead(200, {'Content-Type': 'text/html'})
  res.end(body)
})

module.exports = server
