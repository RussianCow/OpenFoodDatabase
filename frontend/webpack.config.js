const path = require('path')

const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = {
  entry: [
    'babel-polyfill',
    './app/index.jsx'
  ],
  output: {
    publicPath: '/',
    path: path.join(__dirname, 'dist'),
    filename: 'main.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: path.join(__dirname, 'app'),
        loader: 'babel',
        query: {
          presets: ["es2015"],
          cacheDirectory: null
        }
      },
      {
        test: /\.jsx$/,
        include: path.join(__dirname, 'app'),
        loader: 'babel',
        query: {
          presets: ["es2015", "react"],
          cacheDirectory: null
        }
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin(
      [
        {from: 'app/index.html', to: 'index.html', force: true},
      ]
    )
  ],
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  debug: true
}
