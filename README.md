# Open Food Database

The goal of this project is to eventually be a collection of information and quantitative data  about all things related to food. Initially, it's going to be a simple wiki-style site, with the  goal of collecting as much information about food as possible in one place, for professionals and amateurs alike. The long-term vision is for it to become an open data source and API that others can query to build interesting food-related apps and services.

## Tech Stack and Architecture

I've decided on the following high-level architecture:

* [React.js](https://facebook.github.io/react/) on the frontend
* [Python](https://www.python.org/) 3.5 powering the backend API using [Flask](http://flask.pocoo.org/) and [SQLAlchemy](http://www.sqlalchemy.org/)
* [Node.js](https://nodejs.org/) for the web server (more on that below)
* [nginx](http://nginx.org/) as the reverse proxy
* [PostgreSQL](http://www.postgresql.org/) 9.5 as the data store

The reason we have a Node.js web server here at all is for server-side rendering of the React.js components. This way, we are able to use React.js and benefit from all of its advantages without many of the drawbacks of having a JavaScript-powered frontend. There are multiple reasons for using Python for the API server rather than just using Node: 1) I feel more comfortable and productive writing Python than JavaScript; 2) Python is a much more mature language and ecosystem, and things are less likely to break; and 3) the sheer power and flexibility of SQLAlchemy. I'd prefer not to use Node.js at all, but there really is no other sensible way to render React.js components on the server.

## File Structure

* `frontend/app`: the frontend (HTML, CSS, and JavaScript)
* `frontend/server`: the Node.js web server
* `backend`: the Python backend API server
