#!/usr/bin/env bash

export HOST=$(hostname).local
# If your hostname changes, change these values in ~/.profile
echo "export WEB_BASE_URL=http://$HOST/" >> ~/.profile
echo "export API_BASE_URL=http://$HOST/api/v1/" >> ~/.profile
