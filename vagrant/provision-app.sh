#!/usr/bin/env bash

echo "+++ Setting up Python virtual environment +++"
    python3.5 -m venv ~ --without-pip

    # Make sure ~/bin is in our PATH
    source ~/.profile

    # Install pip manually since the PPA doesn't come with it
    wget https://bootstrap.pypa.io/get-pip.py
    python get-pip.py
    rm get-pip.py

    # Install iPython for convenience
    pip install ipython

    cd /vagrant/backend
    pip install -r requirements.txt

    # If a config file doesn't exist, create one
    if [ ! -f config.py ]; then
        cp config.dev.py config.py
    fi

echo "+++ Initializing the database +++"
    python init_db.py

echo "+++ Installing nvm and Node.js +++"
    # This gets rid of npm compile errors when installing modules
    sudo apt-get install -y -q node-gyp

    # Latest NVM install instructions at:
    # https://github.com/creationix/nvm
    curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash

    # Get access to the "nvm" command
    source ~/.nvm/nvm.sh

    # Install latest stable version of node.js
    nvm install 5.10
    nvm alias default 5.10

    # Update npm
    npm install npm -g

echo "+++ Installing npm modules +++"
    cd /vagrant/frontend
    npm install
