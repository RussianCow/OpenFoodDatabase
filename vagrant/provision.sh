#!/usr/bin/env bash

echo "+++ Installing basic dependencies +++"
    echo deb http://apt.postgresql.org/pub/repos/apt/ trusty-pgdg main | \
        tee /etc/apt/sources.list.d/pgdg.list
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
    add-apt-repository ppa:fkrull/deadsnakes -y
    apt-get update -q
    apt-get upgrade -y -q
    apt-get install -y vim git openssl

echo "+++ Installing PostgreSQL 9.5 +++"
    apt-get -y install postgresql-9.5 postgresql-server-dev-9.5
    su - postgres -c "createuser vagrant -d && createdb ofd"
    echo "listen_addresses = '*'" >> /etc/postgresql/9.5/main/postgresql.conf
    echo "host all all 0.0.0.0/0 trust" >> /etc/postgresql/9.5/main/pg_hba.conf
    service postgresql restart

echo "+++ Setting up nginx +++"
    apt-get -y install nginx
    rm /etc/nginx/sites-enabled/default
    ln -s /etc/nginx/sites-available/ofd /etc/nginx/sites-enabled/ofd
    ln -s /vagrant/vagrant/nginx-config /etc/nginx/sites-available/ofd

    # Generate self-signed SSL certificate
    # http://superuser.com/questions/226192/openssl-without-prompt
    mkdir /etc/nginx/ssl
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
        -subj "/" -keyout /etc/nginx/ssl/nginx.key \
        -out /etc/nginx/ssl/nginx.crt

echo "+++ Installing Python 3.5 +++"
    apt-get install -y python3.5 python3.5-dev libffi-dev

echo "+++ Setting up mDNS +++"
    apt-get -y install avahi-daemon
