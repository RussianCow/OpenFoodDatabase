import socket

DEBUG = True

DB_DRIVER = 'psycopg2'
DB_NAME = 'ofd'
DB_HOST = ''
DB_USER = 'vagrant'
DB_PASSWORD = ''

hostname = socket.gethostname()
SERVER_NAME = '{hostname}.local'.format(hostname=hostname)

SECRET_KEY = b'secretdevkey'
