from flask import Blueprint, flash, redirect, render_template, session, url_for
from sqlalchemy.exc import IntegrityError

from ..database import db_session as db
from ..models import User
from ..forms import LoginForm, RegisterForm
from ..utils import get_error_messages_from_form, log_in_as_user, is_logged_in

blueprint = Blueprint('auth', __name__)
