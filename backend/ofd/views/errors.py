def error_403(e):
    # We just return a 404 here for now. We might want to split these up
    # later. Or not.
    return '403', 404


def error_404(e):
    return '404', 404


def error_500(e):
    # Roll back the database in case there's an unfinished transaction
    from ..database import db_session as db
    db.rollback()
    return '500', 500
