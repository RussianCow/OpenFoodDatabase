from flask import g, session

from ofd.database import db_session as db
from ofd.models import User, UserSession


def get_current_user():
    """
    Gets the currently logged in user. If this fails for any reason (the
    user is not logged in, the session is expired, etc) `None` is
    returned.
    """

    try:
        token = session['token']
    except KeyError:
        pass
    else:
        user = User.query.join(UserSession).filter(UserSession.token == token).first()
        return user
    return None


def is_logged_in():
    return g.current_user is not None


def log_in_as_user(user):
    # Create the session in the database
    user_session = UserSession(user=user)
    db.add(user_session)
    db.commit()

    # Create the session in the browser
    session['token'] = user_session.token


def get_error_messages_from_form(form):
    errors = []
    for field_name, field_errors in form.errors.items():
        for field_error in field_errors:
            field_label = getattr(form, field_name).label.text
            error_message = '{}: {}'.format(field_label, field_error)
            errors.append(error_message)
    return errors
