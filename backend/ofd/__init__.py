from flask import Flask
from flask_wtf.csrf import CsrfProtect

from .views import blueprints, errors

app = Flask(__name__)


def configure(config={}):
    app.config.from_object(config)
    CsrfProtect(app)

    # Register all views
    for blueprint in blueprints:
        app.register_blueprint(blueprint)

    app.register_error_handler(403, errors.error_403)
    app.register_error_handler(404, errors.error_404)
    app.register_error_handler(500, errors.error_500)


@app.teardown_appcontext
def remove_db_session(exception=None):
    from .database import db_session as db
    db.remove()
