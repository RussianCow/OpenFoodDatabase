from datetime import datetime
import hashlib
import random
import string

import bcrypt
from sqlalchemy import (Boolean, CHAR, Column, DateTime, Enum, ForeignKey, Integer, LargeBinary,
                        String, Text)
from sqlalchemy.orm import backref, relationship

from .database import Base


article_change_type = Enum('create', 'edit', 'rename', 'delete', name='article_change_type')


def random_token(num_chars=32):
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(num_chars))


class User(Base):
    """
    A user account. For now, must have an email and password. Later,
    other auth methods will be supported.
    """

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False, unique=True, index=True)
    display_name = Column(String, nullable=False)
    password_hash = Column(LargeBinary)

    def __repr__(self):
        return 'User(id=%r, email=%r)' % (self.id, self.email)

    def __setattr__(self, key, value):
        # The preferred way to set a user's password is by setting the
        # model's `password` attribute to the plaintext password, and
        # the encryption is handled automatically and `password_hash` is
        # set.
        if key == 'password':
            self.set_password(value)
        super().__setattr__(key, value)

    def set_password(self, password):
        self.password_hash = self.hash_password(password)

    def password_matches(self, password):
        password_hash = self.hash_password(password, self.password_hash)
        return password_hash == self.password_hash

    def hash_password(self, password, salt=None):
        encoded_value = password.encode('utf-8')
        # We first hash the password with sha512 because bcrypt ignores
        # characters after the first 72.
        sha512_hash = hashlib.sha512(encoded_value).hexdigest().encode('utf-8')
        if salt is None:
            salt = bcrypt.gensalt(rounds=8)
        return bcrypt.hashpw(sha512_hash, salt)


class UserSession(Base):
    """
    A user session.
    """

    __tablename__ = 'user_sessions'

    token = Column(CHAR(32), primary_key=True, default=random_token)
    user_id = Column(Integer, ForeignKey(User.id), nullable=False, index=True)
    created = Column(DateTime, nullable=False, index=True, default=datetime.utcnow)
    last_used = Column(DateTime, nullable=False, index=True, default=datetime.utcnow)

    user = relationship(User, lazy='noload', backref=backref('sessions', lazy='dynamic'))

    def __repr__(self):
        return 'UserSession(token=%r, user_id=%r, created=%r, last_user=%r)' % (self.token,
                                                                                self.user_id,
                                                                                self.created,
                                                                                self.last_used)


class UserLoginAttempt(Base):
    """
    An attempt to log in with an email address. We track this so we can
    try to prevent fraudulent logins in the future.
    """

    __tablename__ = 'user_login_attempts'

    id = Column(Integer, primary_key=True)
    email = Column(String, nullable=False, index=True)
    ip_address = Column(String(15), nullable=False, index=True)
    successful = Column(Boolean, nullable=False, index=True)


class Article(Base):
    __tablename__ = 'articles'

    id = Column(Integer, primary_key=True)
    title = Column(String, nullable=False, unique=True, index=True)
    current_content = Column(String, nullable=False, default='')

    def __repr__(self):
        return 'Article(id=%r, title=%r)' % (self.id, self.title)


class ArticleChange(Base):
    __tablename__ = 'article_changes'

    id = Column(Integer, primary_key=True)
    article_id = Column(Integer, ForeignKey(Article.id), nullable=False, index=True)
    author_id = Column(Integer, ForeignKey(User.id), nullable=False, index=True)
    timestamp = Column(DateTime, nullable=False, index=True)
    revision_number = Column(Integer, nullable=False, index=True)
    type = Column(article_change_type, nullable=False, index=True)
    article_name_change = Column(String)
    content_diff = Column(Text, nullable=False)
    content_snapshot = Column(Text)

    article = relationship(Article, lazy='noload', backref=backref('changes', lazy='dynamic'))

    def __repr__(self):
        return 'ArticleChangeset(id=%r, article_id=%r, author_id=%r, type=%r)' % (self.id,
                                                                                  self.article_id,
                                                                                  self.author_id,
                                                                                  self.type)


class Tag(Base):
    """
    A single-word tag that users can assign to articles.
    """

    __tablename__ = 'tags'

    user_id = Column(Integer, ForeignKey(User.id), primary_key=True)
    article_id = Column(Integer, ForeignKey(Article.id), primary_key=True)
    name = Column(String, primary_key=True)

    user = relationship(User, lazy='noload', backref=backref('tags', lazy='dynamic'))
    article = relationship(Article, lazy='noload', backref=backref('tags', lazy='dynamic'))
