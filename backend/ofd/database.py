import sqlalchemy
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.schema import DropConstraint, DropTable, ForeignKeyConstraint, MetaData, Table

import config

engine = sqlalchemy.create_engine('postgresql+%s://%s:%s@%s/%s' % (
    config.DB_DRIVER, config.DB_USER, config.DB_PASSWORD, config.DB_HOST, config.DB_NAME
), echo=getattr(config, 'DB_DEBUG', config.DEBUG), pool_recycle=3600, pool_size=10)

db_session = scoped_session(sessionmaker(bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


# Loosely borrowed from: http://www.sqlalchemy.org/trac/wiki/UsageRecipes/DropEverything
def drop_everything():
    inspector = sqlalchemy.inspect(engine)
    metadata = MetaData()

    tables = []
    all_fks = []

    for table_name in inspector.get_table_names():
        fks = []
        for fk in inspector.get_foreign_keys(table_name):
            if not fk['name']:
                continue
            fks.append(
                ForeignKeyConstraint((), (), name=fk['name'])
            )
        t = Table(table_name, metadata, *fks)
        tables.append(t)
        all_fks.extend(fks)

    # Drop foreign keys
    for fkc in all_fks:
        db_session.execute(DropConstraint(fkc))
    db_session.commit()

    # Drop tables
    for table in tables:
        db_session.execute(DropTable(table))
    db_session.commit()

    # Drop enums
    enums = inspector.get_enums()
    for enum_dict in enums:
        enum = ENUM(name=enum_dict['name'])
        enum.drop(engine)
    db_session.commit()


def create_all():
    from . import models
    Base.metadata.create_all(engine)
