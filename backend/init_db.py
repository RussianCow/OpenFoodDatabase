from ofd.database import create_all, drop_everything


def init_db():
    drop_everything()
    create_all()

if __name__ == '__main__':
    init_db()
