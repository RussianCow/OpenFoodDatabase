from ofd import app, configure
import config

configure(config)

app.run(host='0.0.0.0', port=8001, use_reloader=True)
