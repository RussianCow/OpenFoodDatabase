#!/bin/ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'

host_hostname = "#{`hostname`}".chomp.chomp('.local')
project = 'ofd'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Prefer to use Parallels on Mac OS X
  config.vm.provider :parallels do |_, override|
    override.vm.box = 'parallels/ubuntu-14.04'
  end

  # Prefer to use LXC on Linux
  config.vm.provider :lxc do |_, override|
    override.vm.box = 'fgrehm/trusty64-lxc'
  end

  # Prefer to use Hyper-V on Windows
  config.vm.provider :hyperv do |_, override|
    override.vm.box = 'pjcolp/trusty64'
  end

  # Support VirtualBox as a fallback
  config.vm.provider :virtualbox do |v, override|
    override.vm.box = 'ubuntu/trusty64'
    # VirtualBox needs a private IP address, otherwise you can't access
    # it from the outside at all
    override.vm.network 'private_network', type: 'dhcp'

    v.cpus = 2
  end

  # The hostname of the VM becomes 'your-hostname-ofd.local'. This
  # requires Bonjour on Windows, which you can extract from the iTunes
  # installer using a utility like 7-Zip.
  config.vm.hostname = "#{host_hostname}-#{project}".downcase

  # Configure vagrant-cachier for apt-get cache
  if Vagrant.has_plugin?('vagrant-cachier')
    config.cache.scope = :box
  end

  config.vm.synced_folder 'frontend/dist', '/var/www/ofd', owner: 'www-data', group: 'www-data'

  # Increase the swapfile size so the VM doesn't choke on VirtualBox
  config.vm.provision 'shell', path: 'vagrant/increase_swap.sh'

  config.vm.provision 'shell', path: 'vagrant/provision.sh'
  config.vm.provision 'shell', path: 'vagrant/provision-app.sh', privileged: false
  config.vm.provision 'shell', path: 'vagrant/set-env-vars.sh', privileged: false

  # We must manually start the nginx service after the shared folder has
  # been mounted
  config.vm.provision :shell, :inline => 'service nginx restart', run: 'always'
end
